import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from  '@angular/forms';
import { Router } from  '@angular/router';
import { AuthService } from "../auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  otpForm : FormGroup;
  isSubmitted  =  false;  
  incorrectOtp = false;
  otpPattern = '^[0-9]*$'
 
  constructor(private authService: AuthService, private router: Router, private formBuilder: FormBuilder) { }
  
  ngOnInit(): void {
    this.loginForm  =  this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
  });

  this.otpForm  =  this.formBuilder.group({
    otp: ['', Validators.required]    
  });
  }

  get formControls() { return this.loginForm.controls; }

  login(userInfo){   
     this.authService.login(userInfo).subscribe((res: any) => {
        this.isSubmitted = true;
        this.authService.setToken(res.authToken);
        // this.router.navigateByUrl('/dashboard');
      }, error => {
        this.isSubmitted = false;
    })    
  }   

  submitOtp(otp){
    console.log(otp, "Otp fetched")
    this.authService.otpVerification(otp).subscribe((res: any) => {
      if(res.status){      
      this.router.navigateByUrl('/dashboard');
      }
    }, error => {
      
  })
  }
}
