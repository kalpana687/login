import {
  Injectable
} from '@angular/core';
import {
  User
} from './user';
import {
  HttpClient,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import {
  environment
} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) {}

  setToken(token) {
    localStorage.setItem('ACCESS_TOKEN', token)
  }

  getToken() {
    return localStorage.getItem('ACCESS_TOKEN')
  }

  public login(userInfo: User) {
    const formdata = new FormData();
    formdata.append('username', userInfo.username);
    formdata.append('password', userInfo.password);
    formdata.append('token', environment.token);
    // localStorage.setItem('ACCESS_TOKEN', "access_token");   
    return this.http.post(`${environment.authApi}latest-backup/api/app/task/login/login`, formdata)
  }

  public isLoggedIn() {
    return localStorage.getItem('ACCESS_TOKEN') !== null;
  }

  public logout() {
    localStorage.removeItem('ACCESS_TOKEN');
  }

  public otpVerification(otp) {
    const formdata = new FormData();
    formdata.append('otp', otp.otp);
    formdata.append('token', environment.token);
    formdata.append('authToken', this.getToken());
    return this.http.post(`${environment.authApi}latest-backup/api/app/task/login/verifyOtp`, formdata)
  }


}
