import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }
  getToken(){
    return localStorage.getItem('ACCESS_TOKEN')    
  }

  getDynamicData() {     
    let header = new HttpHeaders({
      'Authkey': "test-angular-2021"
    });
    const formdata = new FormData();   
    formdata.append('token', environment.token);
    formdata.append('authToken', this.getToken());
    return this.http.post(`${environment.apiUrl}service-api/testangular/api/TestAngular/getDynamicform` ,formdata, { headers: header } ) 
  }
}
