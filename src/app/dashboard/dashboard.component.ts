import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormArray, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { UserService } from '../user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  data: Array < any > = [];  
  dynamicDataForm!: FormGroup;  
  items!: FormArray;

  constructor(private authService: AuthService, private router: Router, private userService: UserService, private formBuilder: FormBuilder) { }
  

  createItem(): FormGroup {  
    return this.formBuilder.group({  
      slab_min: '',  
      slab_max: '',  
      value: ''  
    });  
  }   
  ngOnInit(): void {
    this.dynamicDataForm = this.formBuilder.group({  
      items: this.formBuilder.array([])
    });
    this.getData();
  }

  
  setValues(){
    
  }

  getData() {
    const formGroup = {}
    this.userService.getDynamicData().subscribe((res: any) => {
      this.data = res.data;
      var result = Object.keys(this.data).map((key) => [key, this.data[key]]);
      console.log(result[0][1], "result")
      this.data.forEach(formControl => {       
        formGroup[formControl] = new FormControl("");
      });      

      for (let value in this.data) {
        let formGroupArray = <FormArray>this.dynamicDataForm.controls.items;
        formGroupArray.push(this.setUsersFormArray(formGroup));        
      }
    }, error => {
      
    })
  }

  private setUsersFormArray (formGroup) {
    console.log(formGroup)
    return this.formBuilder.group(formGroup)
  }

  logout(){
    this.authService.logout();
    this.router.navigateByUrl('');
  }

}
